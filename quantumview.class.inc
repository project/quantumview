<?php

/**
 * @file
 * UPS Quantum View API
 */

/*************************************************************************
* Quantum View API                                                       *
*************************************************************************/

/**
 * QuantumView class
 */
class quantumview {

  public function __construct() {
    $this->user = variable_get('quantumview_user', '');
    $this->pass = variable_get('quantumview_pass', '');
    $this->key = variable_get('quantumview_key', '');
    $this->testing = variable_get('quantumview_testing', 0);
    $this->subscription = variable_get('quantumview_subscription', '');
  }

  public function url() {
    if ($this->testing != 1) {
      return 'https://www.ups.com/ups.app/xml/QVEvents';
    } else {
      return 'https://wwwcie.ups.com/ups.app/xml/QVEvents';
    }
  }

  public function quantumtrack() {
    $data = $this->create_quantum_request();
    $url = $this->url();
    $response = $this->request($url, $data);
    return $response;
  }

  public function tracknum($tracking_number) {
    $data = $this->create_num_request($tracking_number);
    $url = $this->url();
    $response = $this->request($url, $data);
    return $response;
  }

  public function request($url, $data) {

    // Assemble the request.
    $headers = array(
      'Content-Type' => 'text/xml; charset=UTF-8',
    );
    $method = 'POST';

    // Send the request.
    $response = drupal_http_request($url, $headers, $method, $data);

    // Convert the response data to XML and return a SimpleXMLElement object.
    return new SimpleXMLElement($response->data);
  }

  /**
   * Generate access request XML. This is included at the top of every other request.
   *
   * @return
   *   Returns XML string output that includes UPS account login information. Returns FALSE if login information isn't available.
   */
  public function access_request() {
    if (!empty($this->key) && !empty($this->user) && !empty($this->pass)) {

      // Create a new AccessRequest XML object.
      $AccessRequest = new SimpleXMLElement('<AccessRequest/>');
      $AccessRequest->addChild('AccessLicenseNumber', $this->key);
      $AccessRequest->addChild('UserId', $this->user);
      $AccessRequest->addChild('Password', $this->pass);
      return $AccessRequest->asXML();
    }
    else {
      return FALSE;
    }
  }

  public function create_quantum_request() {
    $access_xml = $this->access_request();
    if ($access_xml) {

      // Create a new QuantumViewRequest XML object.
      $QuantumViewRequest = new SimpleXMLElement('<QuantumViewRequest/>');
      $QuantumViewRequest->addChild('Request');
      $QuantumViewRequest->Request->addChild('RequestAction', 'QVEvents');

      // Add the subsciption, if it's set.
      if (!empty($this->subscription)) {
        $QuantumViewRequest->addChild('SubscriptionRequest');
        $QuantumViewRequest->SubscriptionRequest->addChild('Name', $this->subscription);
      }

      // Assemble the final output.
      $output = $access_xml . $QuantumViewRequest->asXML();
    }
    else {
      $output = FALSE;
    }
    return $output;
  }

  public function create_num_request($tracking_number) {
    $access_xml = $this->access_request();
    if ($access_xml) {

      // Create a new TrackRequest XML object.
      $TrackRequest = new SimpleXMLElement('<TrackRequest/>');
      $TrackRequest->addChild('Request');
      $TrackRequest->Request->addChild('TransactionReference');
      $TrackRequest->Request->TransactionReference->addChild('CustomerContext');
      $TrackRequest->Request->TransactionReference->CustomerContext->addChild('InternalKey', 'blah');
      $TrackRequest->Request->TransactionReference->addChild('XpciVersion', '1.0');
      $TrackRequest->Request->addChild('RequestAction', 'Track');
      $TrackRequest->addChild('TrackingNumber', $tracking_number);

      // Assemble the final output.
      $output = $access_xml . $TrackRequest->asXML();
    }
    else {
      $output = FALSE;
    }
    return $output;
  }
}