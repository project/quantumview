<?php

/**
 * @file
 * UPS Quantumview API
 */

/*************************************************************************
* Admin pages                                                            *
*************************************************************************/

/**
 * Quantum View settings form.
 */
function quantumview_settings_form($form_state) {
  $form = array();

  // Access License Number
  $form['quantumview_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Access License Number'),
    '#required' => TRUE,
    '#default_value' => variable_get('quantumview_key', ''),
  );

  // Username
  $form['quantumview_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#required' => TRUE,
    '#default_value' => variable_get('quantumview_user', ''),
  );

  // Password
  $form['quantumview_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#required' => TRUE,
    '#default_value' => variable_get('quantumview_pass', ''),
  );

  // Subscription (optional)
  $form['quantumview_subscription'] = array(
    '#type' => 'textfield',
    '#title' => t('Subscription'),
    '#default_value' => variable_get('quantumview_subscription', ''),
  );

  // Testing mode
  $form['quantumview_testing'] = array(
    '#type' => 'radios',
    '#title' => t('Testing Mode'),
    '#required' => TRUE,
    '#options' => array(
      0 => 'Off',
      1 => 'On',
    ),
    '#default_value' => variable_get('quantumview_testing', 0),
  );

  // Automatically download via cron
  $form['quantumview_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-download'),
    '#description' => t('Automatically download unread data files and save them to the database in the background with cron.'),
    '#default_value' => variable_get('quantumview_cron', 0),
  );

  // Return it as a system settings form to handle variable saving.
  return system_settings_form($form);
}